package com.example.desampah.adapter;

public class DataRute {
    private String id_rute;
    private String nama_rute;
    private String detail_rute;

    public String getUsr_id() {
        return usr_id;
    }

    public void setUsr_id(String usr_id) {
        this.usr_id = usr_id;
    }

    private String usr_id;

    public String getId_rute() {
        return id_rute;
    }

    public void setId_rute(String id_rute) {
        this.id_rute = id_rute;
    }

    public String getNama_rute() {
        return nama_rute;
    }

    public void setNama_rute(String nama_rute) {
        this.nama_rute = nama_rute;
    }

    public String getDetail_rute() {
        return detail_rute;
    }

    public void setDetail_rute(String detail_rute) {
        this.detail_rute = detail_rute;
    }
}
