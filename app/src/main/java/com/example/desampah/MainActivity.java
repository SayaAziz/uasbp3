package com.example.desampah;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.desampah.app.AppController;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    ProgressDialog pDialog;
    Button btn_logout, btn_data, btn_rute,btn_status;
    TextView txt_level, txt_username, txt_usr_id, txt_status_sampah, txt_ket_sampah;
    String level, username, warga,usr_id;
    Integer success;
    SharedPreferences sharedpreferences;
    Switch switchSampah;

    private static final String TAG = MainActivity.class.getSimpleName();

    public static final String TAG_LEVEL = "level";
    public static final String TAG_USERNAME = "username";
    public static final String TAG_USR_ID = "usr_id";


    public static final String TAG_NAMA = "nama";
    public static final String TAG_ALAMAT = "alamat";
    public static final String TAG_RUTE = "rute";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    private String url = Server.URL + "read_data.php";
    private String url_sampah = Server.URL + "update_status_sampah.php";
    String tag_json_obj = "json_obj_req";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt_level = (TextView) findViewById(R.id.txt_level);
        txt_username = (TextView) findViewById(R.id.txt_username);
        btn_logout = (Button) findViewById(R.id.btn_logout);
        btn_data = findViewById(R.id.btn_data);
        btn_rute = findViewById(R.id.btn_rute);
        btn_status = findViewById(R.id.btn_status);
        txt_usr_id = findViewById(R.id.txt_usr_id);
        txt_ket_sampah = findViewById(R.id.txt_ket_sampah);
        switchSampah = findViewById(R.id.switch_status);
        txt_status_sampah = findViewById(R.id.txt_status_sampah);

        switchSampah.setChecked(false);
        switchSampah.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    txt_status_sampah.setText("Penuh");
                } else {
                    txt_status_sampah.setText("Kosong");
                }
            }
        });
        if(switchSampah.isChecked()){
            txt_status_sampah.setText("Penuh");
        } else{
            txt_status_sampah.setText("Kosong");
        }

        sharedpreferences = getSharedPreferences(Login.my_shared_preferences, Context.MODE_PRIVATE);

        level = getIntent().getStringExtra(TAG_LEVEL);
        username = getIntent().getStringExtra(TAG_USERNAME);
        usr_id = getIntent().getStringExtra(TAG_USR_ID);

        txt_level.setText(level);
        txt_username.setText("USERNAME : " + username);
        txt_usr_id.setText(usr_id);
        
        warga = "Warga";
        if(level.equals(warga) ) {
            btn_data.setVisibility(View.VISIBLE);
            btn_status.setVisibility(View.VISIBLE);
            btn_rute.setVisibility(View.GONE);
        } else {
            btn_data.setVisibility(View.GONE);
            btn_status.setVisibility(View.GONE);
            txt_ket_sampah.setVisibility(View.GONE);
            btn_rute.setVisibility(View.VISIBLE);
            txt_status_sampah.setVisibility(View.GONE);
            switchSampah.setVisibility(View.GONE);
        }

        btn_rute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RuteActivity.class);
                intent.putExtra(TAG_USR_ID, usr_id);
                startActivity(intent);

            }
        });


        btn_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usr_id = txt_usr_id.getText().toString();
                String status = txt_status_sampah.getText().toString();
                changeStatus(usr_id, status);
            }
        });
        
        btn_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usr_id = txt_usr_id.getText().toString();
                String level = txt_level.getText().toString();
                checkData(usr_id,level);
            }
        });

        btn_logout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // update login session ke FALSE dan mengosongkan nilai id dan username
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean(Login.session_status, false);
                editor.putString(TAG_LEVEL, null);
                editor.putString(TAG_USERNAME, null);
                editor.putString(TAG_USR_ID, null);
                editor.commit();

                Intent intent = new Intent(MainActivity.this, Login.class);
                finish();
                startActivity(intent);
            }
        });
        

    }

    private void changeStatus(final String usr_id, final String status) {
        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Change Data Status Sampah ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, url_sampah, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Change Status Sampah Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getInt(TAG_SUCCESS);

                    // Check for error node in json
                    if (success == 1) {
                        Log.e("Berhasil Edit Status", jObj.toString());
                        Toast.makeText(getApplicationContext(), jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(),
                                jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Change Sampah error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

                hideDialog();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("usr_id", usr_id);
                params.put("status", status);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void checkData(final String usr_id, final String level) {
        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Checking Data ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Check Data Response : " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getInt(TAG_SUCCESS);

                    // Check for error node in json
                    if (success == 1) {
                        String nama = jObj.getString(TAG_NAMA);
                        String alamat = jObj.getString(TAG_ALAMAT);

                        Log.e("Data Ada !", jObj.toString());

                        Toast.makeText(getApplicationContext(), jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();

                        // Memanggil main activity
                        Intent intent = new Intent(MainActivity.this, DataActivity.class);
                        intent.putExtra(TAG_NAMA, nama);
                        intent.putExtra(TAG_ALAMAT, alamat);
                        intent.putExtra(TAG_USR_ID, usr_id);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getApplicationContext(),
                                jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

                hideDialog();

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("usr_id", usr_id);
                params.put("level", level);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
