-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 17, 2020 at 04:09 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id11654343_db_login`
--
CREATE DATABASE IF NOT EXISTS `id11654343_db_login` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `id11654343_db_login`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(25) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tb_biodata`
--

CREATE TABLE `tb_biodata` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `usia` tinyint(4) NOT NULL,
  `domisili` varchar(100) NOT NULL,
  `gambar` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_biodata`
--

INSERT INTO `tb_biodata` (`id`, `nama`, `usia`, `domisili`, `gambar`) VALUES
(12, 'Ana Estianti', 10, 'Malang', 'bambu_runcing.png'),
(3, 'Supandi', 22, 'Madura\r\n', 'bendera_indonesia.png'),
(16, 'Kizaru', 99, 'Jember', 'bendera_indonesia.png'),
(13, 'Mahmudi', 14, 'Surabaya', 'bendera_indonesia.png'),
(20, 'Joni 2', 12, 'Solo', 'bendera_indonesia.png'),
(15, 'Imam Hambali', 30, 'Kalimantan', 'bendera_indonesia.png'),
(17, 'Gawang 22', 11, 'Lapangan', 'bendera_indonesia.png'),
(21, 'abdul aziz', 19, 'solo', 'bendera_indonesia.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_petugas`
--

CREATE TABLE `tb_petugas` (
  `usr_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `id_rute` int(5) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `status_kerja` enum('Pilih','Selesai') NOT NULL,
  `nama_rute` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_petugas`
--

INSERT INTO `tb_petugas` (`usr_id`, `id`, `id_rute`, `nama`, `status_kerja`, `nama_rute`) VALUES
(1, 1, 1, 'Squidward', 'Selesai', 'RUTE A');

-- --------------------------------------------------------

--
-- Table structure for table `tb_rute`
--

CREATE TABLE `tb_rute` (
  `id_rute` int(11) NOT NULL,
  `nama_rute` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `detail_rute` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tb_rute`
--

INSERT INTO `tb_rute` (`id_rute`, `nama_rute`, `detail_rute`) VALUES
(1, 'RUTE A', 'Jalan 1'),
(2, 'RUTE B', 'Belok Kiri'),
(3, 'RUTE C', 'STMIK Adi Unggul Bhirawa\r\nJl. Walanda Maramis No.29, Nusukan, Kec. Banjarsari, Kota Surakarta, Jawa Tengah 57135\r\n\r\nAmbil arah timur di Jl. Krakatau Utara menuju Jl. Walanda Maramis\r\n27 m\r\n\r\nBelok kanan ke Jl. Walanda Maramis\r\n170 m\r\n\r\nAmbil jalur sebelah kanan dan tetap di Jl. Walanda Maramis\r\n160 m\r\n\r\nBelok kiri ke Jl. Tentara Pelajar\r\n400 m\r\n\r\nBelok kanan ke Jl. Semeru Utara IV/Kp. Kandangsapi');

-- --------------------------------------------------------

--
-- Table structure for table `tb_warga`
--

CREATE TABLE `tb_warga` (
  `usr_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `alamat` text NOT NULL,
  `status` enum('Kosong','Penuh') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_warga`
--

INSERT INTO `tb_warga` (`usr_id`, `id`, `nama`, `alamat`, `status`) VALUES
(5, 1, 'LukeS', 'Solo', 'Kosong'),
(6, 2, 'Spongebob', 'Bikini Bottom f', 'Penuh'),
(25, 16, 'Abdul Aziz', 'Jl. Brigjen Katamso No.55, Tegalharjo, Kec. Jebres, Kota Surakarta, Jawa Tengah 57128', 'Penuh');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `usr_id` int(5) NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `level` enum('Admin','Warga','Petugas') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`usr_id`, `username`, `password`, `level`) VALUES
(1, 'squidward', 'squid12345', 'Petugas'),
(6, 'spongebob', 'bob12345', 'Warga'),
(25, 'aziz', 'aziz123', 'Warga');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_biodata`
--
ALTER TABLE `tb_biodata`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_petugas`
--
ALTER TABLE `tb_petugas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_rute`
--
ALTER TABLE `tb_rute`
  ADD PRIMARY KEY (`id_rute`);

--
-- Indexes for table `tb_warga`
--
ALTER TABLE `tb_warga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`usr_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_biodata`
--
ALTER TABLE `tb_biodata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tb_petugas`
--
ALTER TABLE `tb_petugas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_rute`
--
ALTER TABLE `tb_rute`
  MODIFY `id_rute` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_warga`
--
ALTER TABLE `tb_warga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `usr_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
